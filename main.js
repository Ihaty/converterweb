let selectMode = document.getElementById("selectMode");

let selectFromTemperature = document.getElementById("selectFromTemperature");
let selectToTemperature = document.getElementById("selectToTemperature");

let selectFromLength = document.getElementById("selectFromLength");
let selectToLength = document.getElementById("selectToLength");

let selectFromWeight = document.getElementById("selectFromWeight");
let selectToWeight = document.getElementById("selectToWeight");

let inputFrom = document.getElementById("inputFrom");
let inputTo = document.getElementById("inputTo");

function setDisplay() {
    let value = selectMode.value;
    if (value == "Температура") {
        document.getElementById('selectLengthContainer').style.display = "none";
        document.getElementById('selectWeightContainer').style.display = "none";
        document.getElementById('selectTemperatureContainer').style.display = "flex";
        document.getElementById("buttonTemperature").style.display = "flex";
        document.getElementById("buttonLength").style.display = "none";
        document.getElementById("buttonWeight").style.display = "none";

    } else if (value == "Длина") {
        document.getElementById('selectTemperatureContainer').style.display = "none";
        document.getElementById('selectWeightContainer').style.display = "none";
        document.getElementById('selectLengthContainer').style.display = "flex";
        document.getElementById("buttonLength").style.display = "flex";
        document.getElementById("buttonTemperature").style.display = "none";
        document.getElementById("buttonWeight").style.display = "none";
    } else {
        document.getElementById('selectLengthContainer').style.display = "none";
        document.getElementById('selectTemperatureContainer').style.display = "none";
        document.getElementById('selectWeightContainer').style.display = "flex";
        document.getElementById("buttonWeight").style.display = "flex";
        document.getElementById("buttonLength").style.display = "none";
        document.getElementById("buttonTemperature").style.display = "none";
    }
}

function convertedTemperature() {
    let valueFrom = selectFromTemperature.value;
    let valueTo = selectToTemperature.value
    if (valueFrom == valueTo) {
        inputTo.value = inputFrom.value;
    } else if (selectFromTemperature.value === "Кельвин" && selectToTemperature.value === "Цельсий") {
        inputTo.value = inputFrom.value - 273.15;
    } else if (selectFromTemperature.value === "Кельвин" && selectToTemperature.value === "Фаренгейт") {
        inputTo.value = (inputFrom.value - 273.15) * 9/5 + 32;
    } else if (selectFromTemperature.value === "Цельсий" && selectToTemperature.value === "Кельвин") {
        inputTo.value = +inputFrom.value + 273.15;
    } else if (selectFromTemperature.value === "Цельсий" && selectToTemperature.value === "Фаренгейт") {
        inputTo.value = (inputFrom.value * 9/5) + 32;
    } else if (selectFromTemperature.value === "Фаренгейт" && selectToTemperature.value === "Цельсий") {
        inputTo.value = (inputFrom.value - 32) * 5/9
    } else if (selectFromTemperature.value=== "Фаренгейт" && selectToTemperature.value === "Кельвин") {
        inputTo.value = (inputFrom.value - 32) * 5/9 + 273.15
    }
}

function convertedLength() {
    let valueFrom = selectFromLength.value;
    let valueTo = selectToLength.value;
    if (valueFrom == valueTo) {
        inputTo.value = inputFrom.value;
    } else if (selectFromLength.value == "Метры" && selectToLength.value == "Футы") {
        inputTo.value = inputFrom.value * 3.281;
    } else if (selectFromLength.value == "Метры" && selectToLength.value == "Мили") {
        inputTo.value = inputFrom.value / 1609;
    } else if (selectFromLength.value == "Футы" && selectToLength.value == "Метры") {
        inputTo.value = inputFrom.value / 3.281;
    } else if (selectFromLength.value == "Футы" && selectToLength.value == "Мили") {
        inputTo.value = inputFrom.value / 5280;
    } else if (selectFromLength.value == "Мили" && selectToLength.value == "Метры") {
        inputTo.value = inputFrom.value * 1609
    } else if (selectFromLength.value == "Мили" && selectToLength.value == "Футы") {
        inputTo.value = inputFrom.value * 5280;
    }
}

function convertedWeight() {
    let valueFrom = selectFromWeight.value;
    let valueTo = selectToWeight.value;
    if (valueFrom == valueTo) {
        inputTo.value = inputFrom.value;
    } else if (selectFromWeight.value == "КГ" && selectToWeight.value == "LBS") {
        inputTo.value = inputFrom.value * 2.205;
    } else if (selectFromWeight.value == "КГ" && selectToWeight.value == "Short Ton") {
        inputTo.value = inputFrom.value / 907;
    } else if (selectFromWeight.value == "LBS" && selectToWeight.value == "КГ") {
        inputTo.value = inputFrom.value / 2.205;
    } else if (selectFromWeight.value == "LBS" && selectToWeight.value == "Short Ton") {
        inputTo.value = inputFrom.value / 2000;
    } else if (selectFromWeight.value == "Short Ton" && selectToWeight.value == "КГ") {
        inputTo.value = inputFrom.value * 907;
    } else if (selectFromWeight.value == "Short Ton" && selectToWeight.value == "LBS") {
        inputTo.value = inputFrom.value * 2000;
    }
}
